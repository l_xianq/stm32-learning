## DMA控制器
#### 1.作用
* 减轻cpu的负担，能不经过cpu的处理传输数据，把数据从外设到存储器，从存储器到外设，从存储器到存储器
#### 2.通道配置过程
1. 在DMA_CPARx寄存器中设置外设寄存器的地址。发生外设数据传输请求时，这个地址将是
数据传输的源或目标。
2. 在DMA_CMARx寄存器中设置数据存储器的地址。发生外设数据传输请求时，传输的数据将
从这个地址读出或写入这个地址。
3. 在DMA_CNDTRx寄存器中设置要传输的数据量。在每个数据传输后，这个数值递减。
4. 在DMA_CCRx寄存器的PL[1:0]位中设置通道的优先级。
5. 在DMA_CCRx寄存器中设置数据传输的方向、循环模式、外设和存储器的增量模式、外设
和存储器的数据宽度、传输一半产生中断或传输完成产生中断。
6. 设置DMA_CCRx寄存器的ENABLE位，启动该通道。
一旦启动了DMA通道，它既可响应联到该通道上的外设的DMA请求。
当传输一半的数据后，半传输标志(HTIF)被置1，当设置了允许半传输中断位(HTIE)时，将产生
一个中断请求。在数据传输结束后，传输完成标志(TCIF)被置1，当设置了允许传输完成中断位
(TCIE)时，将产生一个中断请求。
> 选自stm32参考手册

#### 3.标准库函数的配置
```
void MYDMA_Config(DMA_Channel_TypeDef* DMA_CHx,u32 cpar,u32 cmar,u16 cndtr)   //以usart2_RX为例
{
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);	//使能DMA传输
    DMA_DeInit(DMA_CHx);             //将DMA的通道1寄存器重设为缺省值
	DMA_InitStructure.DMA_PeripheralBaseAddr = cpar;       //DMA外设ADC基地址
	DMA_InitStructure.DMA_MemoryBaseAddr = cmar;           //DMA内存基地址
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;     //数据传输方向，从外设读取发送到内存，外设为源
	DMA_InitStructure.DMA_BufferSize = cndtr;              //DMA通道的DMA缓存的大小，设置传输的数据量
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;  //外设地址寄存器不变
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;  //内存地址寄存器递增
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;  //数据宽度为8位
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; //数据宽度为8位
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;  //工作在正常缓存模式
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium; //DMA通道 x拥有中优先级 
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输
	DMA_Init(DMA_CHx, &DMA_InitStructure);  
    //根据DMA_InitStruct中指定的参数初始化DMA的通道USART2_Rx_DMA_Channel所标识的寄存器

} 

void MYDMA_Enable(DMA_Channel_TypeDef*DMA_CHx)  //用来发送数据，当传输1次数据后，想要再传输1次，需要重新给CNDTR寄存器重新幅值
{ 
	DMA_Cmd(DMA_CHx, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
 	DMA_SetCurrDataCounter(DMA1_Channe6,DMA1_MEM_LEN);//DMA通道的DMA缓存的大小
 	DMA_Cmd(DMA_CHx, ENABLE);     //使能USART1 TX DMA1 所指示的通道 
}	

//还需要在串口初始化时使能串口2的DMA接收
USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);   //使能串口2 DMA接收
```
## 串口空闲IDLE中断初始化的配置
` USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);   //使能串口2 DMA接收 ,加上这行就行了`
## 串口中断函数配置
```
void USART2_IRQHandler(void)                	//串口1中断服务程序
{
	u16 Res;
    if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)  //接收中断
	{
		
	    Res=USART2->SR;
        Res=USART2->DR;                        //清USART_IT_IDLE标志
        DMA_Cmd(DMA1_Channel6,DISABLE);    //关闭DMA
        DMA1_Channel6->CNDTR=5;       //重新设置接收数据个数        
	    DMA_Cmd(DMA1_Channel6,ENABLE);  //开启DMA
	    if(USART_RX_BUF[4]=='a')               //数据标志位
	    {
	        kp=(unsigned int*)(USART_RX_BUF);
            USART_RX_STA = 1;           //接收数据标志位置1
	        printf("ok\n");
	        USART_RX_BUF[4]=0;
	    }
	    else 
	    {
	        printf("error\n");
	    }
    }
}
```
