## 1.接收

   (1)采用指针的强制类型转换
```
	char x[8];
	unsigned int *a;
	x[0] = 1; x[1] = 1; x[2] = 0; x[3] = 0;
	x[4] = 0; x[5] = 0; x[6] = 1; x[7] = 0;
	a = (int*)x;                         
	printf("%d  %d ", *a,*(a+1));  
```
  （2）采用共用体
```
     void long_char(unsigned long l,unsigned char *s)
    {undefined
    union change
    {undefined
        long d;
        unsigned char dat[4];
    }r1;
     r1.d = l;
    *s = r1.dat[0];
    *(s+1) = r1.dat[1];
    *(s+2) = r1.dat[2];
    *(s+3) = r1.dat[3];  
    }   
```
## 2.发送

（1）采用指针的转换
```
    void float_char(float f,unsigned char *s)
    {undefined
    unsigned char *p;
    p = (unsigned char *)&f;
    *s = *p;
    *(s+1) = *(p+1);
    *(s+2) = *(p+2);
    *(s+3) = *(p+3);
    }
```
（2）采用共用体
## 串口调参
* 1.下位机代码配置
```
u8 USART_RX_BUF[5];  //创建个u8类型的数组，数组最后一位为标识位
unsigned int *kp,*kd;
**********************************************************************
//在串口中断中
u16 Res;
if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)  //接收中断
{		
	Res=USART2->SR;
    Res=USART2->DR;                        //清USART_IT_IDLE标志
    DMA_Cmd(DMA1_Channel6,DISABLE);    //关闭DMA
    DMA1_Channel6->CNDTR=5;       //重新设置接收数据个数        
	DMA_Cmd(DMA1_Channel6,ENABLE);  //开启DMA
	if(USART_RX_BUF[4]=='a')               //数据标志位,用来判断输入的数据对应那个
	{
	    kp=(unsigned int*)(USART_RX_BUF);
        USART_RX_STA = 1;           //接收数据标志位置1
	    printf("ok\n");
	    USART_RX_BUF[4]=0;
	}
    else if(USART_RX_BUF[4]=='b')
    {
        ki=(unsigned int*)(USART_RX_BUF);
        USART_RX_STB = 1;
        printf("ok\n");
	    USART_RX_BUF[4]=0;
    }
	else 
	{
	    printf("error\n");
	}
}
**********************************************************
//在主函数中
int main()
{
    while(USART_RX_STB==0||USART_RX_STA==0);       //卡在此处，等待kp，ki参数的输入 
}

```
* 2.上位机串口助手发送数据格式
![发送格式](1.jpg.png)


在stm32中是采用小端模式存储数据的。什么是小端模式？

大端模式：是指数据的高字节保存在内存的低地址中，而数据的低字节保存在内存的高地址端。

小端模式，是指数据的高字节保存在内存的高地址中，低位字节保存在在内存的低地址端。

按图片所示样例，stm32接收数据后USART_RX_BUF[5]={1，1，0，0，97};按照小端模式存储，

得到的值为0*(256^3)+0*(256^2)+1*256+1=257;


